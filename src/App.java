import com.devcamp.javabasic_j04.s10.Employee;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Employee employee1 = new Employee();
        Employee employee2 = new Employee(01, "Ngo", "Nghia", 10000000);
        System.out.println(employee1.toString());
        System.out.println(employee2.toString());
        int luonghangnam1 = employee1.getAnnualSalary();
        int luonghangnam2 = employee2.getAnnualSalary();
        System.out.println(luonghangnam1);
        System.out.println(luonghangnam2);

        int luongtanglen01 = employee1.raiseSalary(2);
        int luongtanglen02 = employee2.raiseSalary(1);
        System.out.println("Tien luong tang len cuar nguoi 1 la :" + luongtanglen01);
        System.out.println("Tien luong tang len cuar nguoi 2 la :" + luongtanglen02);

        int tongluong = luonghangnam1 + luongtanglen01;
        int tongluong02 = luonghangnam2 + luongtanglen02;

        System.out.println("Luong thuc nhan nguoi 1: " + tongluong);
        System.out.println("Luong thuc nhan nguoi 1: " + tongluong02);

    }
}
