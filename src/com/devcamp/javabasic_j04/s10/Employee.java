package com.devcamp.javabasic_j04.s10;

public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;

    public Employee() {
        this.id = 022;
        this.firstName = "Quy";
        this.lastName = "Nguyen";
        this.salary = 1000000;
    }

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String setName() {
        return String.format(firstName, lastName);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(int percent) {
        return salary * percent / 100;
    }

    @Override
    public String toString() {
        return String.format("Employee[id= %s, name =%s %s, salary= %s", id, firstName, lastName, salary);
    }
}
